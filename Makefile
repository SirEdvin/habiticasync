lint:
	pylint habiticasync
	pycodestyle habiticasync
front-lint:
	ng lint
inspect:
	pipenv check
	bandit -r -c .bandit habiticasync 
pytest:
	@echo "Where is my tests?!"
	# pytest tests