__author__ = "Gladyshev Bogdan"
__copyright__ = "Copyright 2019, SirEdvin"
__credits__ = ["Gladyshev Bogdan"]
__license__ = "MIT"
__version__ = "0.1.0"
__maintainer__ = "Gladyshev Bogdan"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Production"
