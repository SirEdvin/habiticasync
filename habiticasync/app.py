import asyncio
import logging

import aiojobs
import sanic
from sanic_prometheus import monitor
from sanic_service_utils.common import (
    log_configuration, sentry_configuration,
    aiohttp_session_configuration
)

from habiticasync.services import HabiticaService, TaigaService

from habiticasync.apps.taiga import taiga_app

__author__ = "Gladyshev Bogdan"
__copyright__ = "Copyright 2019, SirEdvin"
__credits__ = ["Gladyshev Bogdan"]
__license__ = "MIT"
__version__ = "0.1.0"
__maintainer__ = "Gladyshev Bogdan"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Production"
_log = logging.getLogger(__name__)


app = sanic.Sanic('habiticasync')

monitor(app).expose_endpoint()

app.blueprint(log_configuration)
app.blueprint(sentry_configuration)
app.blueprint(aiohttp_session_configuration)

app.blueprint(taiga_app)


@app.listener('after_server_start')
async def habitica_init(sanic_app, _loop) -> None:
    await asyncio.gather(
        HabiticaService.init(sanic_app),
        TaigaService.init(sanic_app)
    )


@app.listener('before_server_start')
async def start_scheduler(sanic_app, _loop) -> None:

    def exception_handler(_, context):
        _log.error(
            "Exception %s with message %s occurs in job %s",
            context.get('message', ''),
            context.get('exception', ''),
            context.get('job', '')
        )

    sanic_app.scheduler = await aiojobs.create_scheduler(
        exception_handler=exception_handler
    )


@app.listener('after_server_stop')
async def stop_scheduler(sanic_app, _loop) -> None:
    await sanic_app.scheduler.close()
