import logging
import typing
from urllib.parse import urljoin

import aiohttp

from .habitica import HabiticaService, HabiticaChecklistItem, HabiticaTodo

__author__ = "Gladyshev Bogdan"
__copyright__ = "Copyright 2019, SirEdvin"
__credits__ = ["Gladyshev Bogdan"]
__license__ = "MIT"
__version__ = "0.1.0"
__maintainer__ = "Gladyshev Bogdan"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Production"
__all__ = ['TaigaService']
_log = logging.getLogger(__name__)


class TaigaClient:

    __slots__ = ('session', 'token', 'host')

    def __init__(self, host: str, session: aiohttp.ClientSession) -> None:
        self.session = session
        self.host = host
        self.token = None

    def _build_headers(self):
        return {
            'Authorization': f'Bearer {self.token}',
            'X-Disable-Pagination': 'True'
        }

    async def login(self, username: str, password: str):
        data = {
            'username': username,
            'password': password,
            'type': 'normal'
        }
        async with self.session.post(urljoin(self.host, 'api/v1/auth'), json=data) as resp:
            result = await resp.json()
            self.token = result['auth_token']

    async def projects(self):
        async with self.session.get(urljoin(self.host, 'api/v1/projects'), headers=self._build_headers()) as resp:
            if resp.status != 200:
                return None
            result = await resp.json()
        return result

    async def user_stories(self, **params):
        async with self.session.get(
                urljoin(self.host, 'api/v1/userstories'), headers=self._build_headers(), params=params) as resp:
            if resp.status != 200:
                return None
            result = await resp.json()
        return result

    async def tasks(self, **params):
        async with self.session.get(
                urljoin(self.host, 'api/v1/tasks'), headers=self._build_headers(), params=params) as resp:
            if resp.status != 200:
                return None
            result = await resp.json()
        return result

    async def user_story(self, user_story_id: str):
        async with self.session.get(
                urljoin(self.host, f'api/v1/userstories/{user_story_id}'), headers=self._build_headers()) as resp:
            if resp.status != 200:
                return None
            result = await resp.json()
        return result

    async def task(self, task_id: str):
        async with self.session.get(
                urljoin(self.host, f'api/v1/tasks/{task_id}'), headers=self._build_headers()) as resp:
            if resp.status != 200:
                return None
            result = await resp.json()
        return result


class TaigaService:

    owner: str
    client: TaigaClient

    @classmethod
    async def init(cls, sanic_app):
        cls.owner = sanic_app.config.get('TAIGA_USER')
        cls.client = TaigaClient(sanic_app.config.get('TAIGA_HOST'), sanic_app.async_session)
        await cls.client.login(sanic_app.config.get('TAIGA_USER'), sanic_app.config.get('TAIGA_PASSWORD'))

    @classmethod
    async def project_list(cls):
        return await cls.client.projects()

    @classmethod
    async def delete_user_story(cls, user_story: typing.Dict) -> None:
        return await HabiticaService.ensure_todo_deleted(cls._build_entity_alias(user_story))

    @classmethod
    async def handle_user_story(cls, user_story: typing.Union[str, typing.Dict], source: str = 'api') -> None:
        if source == 'id':
            user_story = await cls.client.user_story(user_story)
        if source == 'webhook':
            user_story['assigned_to_extra_info'] = user_story['assigned_to']
            user_story['status_extra_info'] = user_story['status']
        task_alias = cls._build_entity_alias(user_story)
        if user_story['is_closed']:
            _log.info(
                'Close task %s (%s), because it closed in taiga',
                task_alias,
                user_story['subject']
            )
            return await HabiticaService.ensure_todo_closed_by_id(task_alias)
        if user_story['assigned_to_extra_info'] is None or \
                user_story['assigned_to_extra_info']['username'] != cls.owner:
            user_story_tasks = await cls.client.tasks(user_story=user_story['id'])
            if not user_story_tasks or not any(
                    filter(lambda task: task['assigned_to_extra_info']['username'] == cls.owner, user_story_tasks)):
                _log.info(
                    'Delete task %s (%s), because it not bound to defined user',
                    task_alias,
                    user_story['subject']
                )
                return await HabiticaService.ensure_todo_deleted(task_alias)
        if source == 'api':
            user_story = await cls.client.user_story(user_story['id'])
        await cls.ensure_user_story(user_story)

    @classmethod
    def _build_entity_alias(cls, entity, entity_type='US'):
        project = (entity['project'] if isinstance(entity['project'], int) else entity['project']['id'])
        return f"taiga_kf_{project}_{entity_type}_{entity['id']}"

    @classmethod
    async def _handle_user_story_close(cls, user_story: typing.Dict, done: bool = False):
        pass

    @classmethod
    async def ensure_user_story(cls, user_story: typing.Dict):
        task_alias = cls._build_entity_alias(user_story)
        if not user_story['milestone'] and user_story['status_extra_info']['name'] == 'New':
            _log.info(
                'Delete task %s (%s), because it not has milestone and has new status',
                task_alias,
                user_story['subject']
            )
            return await HabiticaService.ensure_todo_deleted(task_alias)
        habitica_todo = HabiticaTodo(
            alias=task_alias,
            text=user_story['subject'],
            notes=user_story['description'],
            checklist=await cls._fetch_checklist(user_story),
            is_closed=user_story['is_closed']
        )
        _log.info(
            'Ensure about task %s (%s)',
            task_alias,
            user_story['subject']
        )
        await HabiticaService.ensure_todo(habitica_todo)

    @classmethod
    async def _fetch_checklist(cls, user_story: typing.Dict):
        checklist = []
        for subtask in await cls.client.tasks(user_story=user_story['id']):
            if subtask['assigned_to_extra_info'] and subtask['assigned_to_extra_info']['username'] == cls.owner:
                checklist.append(
                    HabiticaChecklistItem(
                        text=subtask['subject'],
                        is_closed=subtask['is_closed']
                    )
                )
        return checklist
