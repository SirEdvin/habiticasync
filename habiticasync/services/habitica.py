import logging
import typing

import aiohttp
import attr
import habitipy.aio

__author__ = "Gladyshev Bogdan"
__copyright__ = "Copyright 2019, SirEdvin"
__credits__ = ["Gladyshev Bogdan"]
__license__ = "MIT"
__version__ = "0.1.0"
__maintainer__ = "Gladyshev Bogdan"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Production"
__all__ = [
    'HabiticaService',
    'HabiticaChecklistItem', 'HabiticaTodo'
]
_log = logging.getLogger(__name__)


@attr.s(auto_attribs=True, slots=True)
class HabiticaChecklistItem:  # pylint: disable=too-few-public-methods
    text: str
    id: typing.Optional[str] = None
    is_closed: bool = False


@attr.s(auto_attribs=True, slots=True)
class HabiticaTodo:  # pylint: disable=too-few-public-methods
    alias: str
    text: str
    notes: str
    checklist: typing.List[HabiticaChecklistItem]
    is_closed: bool = False
    id: typing.Optional[str] = None


class HabiticaService:

    client: habitipy.aio.HabitipyAsync
    session: aiohttp.ClientSession

    @classmethod
    async def init(cls, sanic_app):
        cls.session = sanic_app.async_session
        cls.client = habitipy.aio.HabitipyAsync({
            'url': 'https://habitica.com',
            'login': sanic_app.config.get('HABITICA_USER'),
            'password': sanic_app.config.get('HABITICA_PASSWORD'),
            'show_numbers': True,
            'show_style': 'wide'
        })

    @classmethod
    async def ensure_todo_closed_by_id(cls, todo_alias: str):
        try:
            task_data = await cls.client.tasks[todo_alias].get(cls.session)
            if not task_data['completed']:
                await cls.client.tasks[todo_alias].score['up'].post(cls.session)
        except aiohttp.client_exceptions.ClientResponseError as exc:
            if exc.code != 404:
                raise

    @classmethod
    async def ensure_todo_closed(cls, todo: HabiticaTodo):
        try:
            task_data = await cls.client.tasks[todo.alias].get(cls.session)
            if not task_data['completed']:
                await cls.client.tasks[todo.alias].score['up'].post(cls.session)
        except aiohttp.client_exceptions.ClientResponseError as exc:
            if exc.code != 404:
                raise

    @classmethod
    async def ensure_todo_deleted(cls, todo_alias: str):
        try:
            await cls.client.tasks[todo_alias].delete(cls.session)
        except aiohttp.client_exceptions.ClientResponseError:
            pass

    @classmethod
    async def ensure_todo_opened(cls, todo: HabiticaTodo):
        habitica_todo = cls.client.tasks[todo.alias]
        try:
            habitica_data = await habitica_todo.get(cls.session)
        except aiohttp.client_exceptions.ClientResponseError as exc:
            if exc.code != 404:
                raise
            habitica_data = None
        if habitica_data is None:
            _log.debug('Create task %s, because it not exists', todo)
            await cls.client.tasks.user.post(
                cls.session,
                text=todo.text,
                notes=todo.notes,
                alias=todo.alias,
                type='todo'
            )
            habitica_data = await habitica_todo.get(cls.session)
        else:
            change_dict = {}
            if habitica_data['text'] != todo.text:
                change_dict['text'] = todo.text
            if habitica_data['notes'] != todo.notes:
                change_dict['notes'] = todo.notes
            if change_dict:
                _log.debug('Update %s in task %s', change_dict, todo.alias)
                await habitica_todo.put(cls.session, **change_dict)
        await cls._ensure_subtasks(habitica_todo, habitica_data, todo)

    @classmethod
    async def ensure_todo(cls, todo: HabiticaTodo):
        if todo.is_closed:
            return await cls.ensure_todo_closed(todo)
        return await cls.ensure_todo_opened(todo)

    @classmethod
    async def _ensure_subtasks(cls, habitica_todo, habitica_data, todo: HabiticaTodo):
        for checklist_item in todo.checklist:
            exist_checklist_item = next(
                filter(lambda x: x['text'] == checklist_item.text, habitica_data['checklist']),  # pylint: disable=cell-var-from-loop
                None
            )
            if exist_checklist_item is None:
                checklist_item = HabiticaChecklistItem(
                    id=(await habitica_todo.checklist.post(cls.session, text=checklist_item.text))['_id'],
                    text=checklist_item.text,
                    is_closed=checklist_item.is_closed
                )
            else:
                checklist_item.id = exist_checklist_item['id']
            if checklist_item.is_closed:
                if exist_checklist_item and exist_checklist_item['completed']:
                    continue
                try:
                    await habitica_todo.checklist[checklist_item.id].score.post(cls.session)
                except aiohttp.client_exceptions.ClientResponseError:
                    _log.exception("Exception when try to score subtask")
        for checklist_element in habitica_data['checklist']:
            if not any(filter(lambda x: checklist_element['text'] == x.text, todo.checklist)):  # pylint: disable=cell-var-from-loop
                await habitica_todo.checklist[checklist_element['id']].delete(cls.session)
