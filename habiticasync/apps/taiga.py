import hashlib
import hmac
import logging

import sanic
import sanic.exceptions
import sanic.response
import sanic.request

from habiticasync.services import TaigaService

__author__ = "Gladyshev Bogdan"
__copyright__ = "Copyright 2019, SirEdvin"
__credits__ = ["Gladyshev Bogdan"]
__license__ = "MIT"
__version__ = "0.1.0"
__maintainer__ = "Gladyshev Bogdan"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Production"
_log = logging.getLogger(__name__)

taiga_app = sanic.Blueprint('taiga_app', url_prefix='/taiga')  # pylint: disable=invalid-name


@taiga_app.post('/resync')
async def resync(request: sanic.request.Request) -> sanic.response.HTTPResponse:
    if request.args.get('key', '') != request.app.config.get('TAIGA_SECRET_KEY'):
        raise sanic.exceptions.NotFound('Not found')
    project_list = await TaigaService.project_list()
    if request.args.get('filter[userstory]'):
        us_limitation = [int(x) for x in request.args.get('filter[userstory]').split(',')]
        for user_story_id in us_limitation:
            await TaigaService.handle_user_story(await TaigaService.client.user_story(user_story_id))
    else:
        if request.args.get('filter[project]'):
            project_limitation = [int(x) for x in request.args.get('filter[project]').split(',')]
        else:
            project_limitation = None
        for project in project_list:
            if not project_limitation or project['id'] in project_limitation:
                _log.info('Processs project %s (%s)', project['id'], project["name"])
                for user_story in await TaigaService.client.user_stories(project=project['id']):
                    await TaigaService.handle_user_story(user_story)
    return sanic.response.json({'detail': 'processed'})


def verify_signature(request):
    mac = hmac.new(request.app.config.get('TAIGA_SECRET_KEY').encode("utf-8"), msg=request.body, digestmod=hashlib.sha1)
    return mac.hexdigest() == request.headers['X-TAIGA-WEBHOOK-SIGNATURE']


@taiga_app.post('/webhook')
async def webhook(request: sanic.request.Request) -> sanic.response.HTTPResponse:
    if not verify_signature(request):
        raise sanic.exceptions.NotFound('Not found')
    hook_data = request.json
    if hook_data['action'] == 'test':
        return sanic.response.json({'detail': 'ping'})
    if hook_data['type'] == 'task':
        await request.app.scheduler.spawn(
            TaigaService.handle_user_story(hook_data['data']['user_story']['id'], source='id')
        )
    if hook_data['type'] == 'userstory':
        if hook_data['action'] == 'delete':
            await request.app.scheduler.spawn(TaigaService.delete_user_story(hook_data['data']))
        else:
            await request.app.scheduler.spawn(TaigaService.handle_user_story(hook_data['data'], source='webhook'))
    return sanic.response.json({'detail': 'processed'})
