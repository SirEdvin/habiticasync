FROM python:3.7

ENV DOCKERIZE_VERSION=v0.6.1
ADD https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz /dockerize.tar.gz

RUN python3 -m pip install --upgrade setuptools pip pipenv && \
    tar -C /usr/local/bin -xzvf /dockerize.tar.gz && rm -f /dockerize.tar.gz && \
    mkdir -p /app/metrics

WORKDIR /app
COPY ["Pipfile.lock", "Pipfile", "manage.py", "dockerfiles/gunicorn_conf.py", "/app/"]

COPY "habiticasync/" "/app/habiticasync"

RUN pipenv install --system --deploy

EXPOSE 5000
ENV GUNICORN_WORKERS_COUNT 2
ENV GUNICORN_BIND 0.0.0.0:5000
ENV GUNICORN_TIMEOUT 300
ENV prometheus_multiproc_dir /app/metrics

CMD ["/bin/sh", "-c", "/usr/local/bin/gunicorn -c gunicorn_conf.py habiticasync.app:app"]