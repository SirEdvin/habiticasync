#!/usr/bin/env sh
set -x
set -e

function dockerize_wrap() {
    dockerize "$@"
}

dockerize_wrap "$@"