import os

from prometheus_client import multiprocess


LOG_TIMESTAMP_FORMAT = "%d/%b/%Y:%H:%M:%S %z"


def child_exit(server, worker):
    multiprocess.mark_process_dead(worker.pid)


bind = os.environ.get('GUNICORN_BIND', '0.0.0.0:5666')
workers = os.environ.get('GUNICORN_WORKERS_COUNT', 2)
pid = '/run/gunicorn.pid'
timeout = os.environ.get('GUNICORN_TIMEOUT', 300)
forwarded_allow_ips = os.environ.get('GUNICORN_FORWARDED_ALLOW_IPS', "*")
capture_output = True
worker_class = 'sanic.worker.GunicornWorker'
log_level = 'INFO'
