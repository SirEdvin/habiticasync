#!/usr/bin/env python3
import asyncio

from sanic_service_utils.command import CommandEnv
from habiticasync.app import app

__author__ = "Bogdan Gladyshev"
__copyright__ = "Copyright 2018, Forza"
__credits__ = ["Forza"]
__license__ = "Proprietary"
__version__ = "0.10.1"
__maintainer__ = "Bogdan Gladyshev"
__email__ = "b.gladyshev@kf.ua"
__status__ = "Production"


if __name__ == "__main__":
    CommandEnv.app = app
    CommandEnv.default_port = '5000'
    CommandEnv.run_cli()
